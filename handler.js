'use strict';

module.exports.endpoint = (event, context, callback) => {
    const date1 = new Date().getTime();
    const date2 = new Date("12/25/2019");
    const diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));
    const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: `Hallo die aktuelle Zeit ist ${new Date().toUTCString()}, es sind noch ${diffDays} Tage bis Weihnachten!`,
    }),
  };

  callback(null, response);
};
